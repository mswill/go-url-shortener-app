package main

import (
	"bytes"
	"fmt"
	"github.com/vmihailenco/msgpack/v5"
	"go-url-shortener-app/shortner"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

func httpPort() string {
	port := "9090"
	if os.Getenv("PORT") != "" {
		port = os.Getenv("PORT")
	}
	return fmt.Sprintf(":%s", port)
}

func main() {
	address := fmt.Sprintf("http://localhost%s", httpPort())
	redirect := shortner.Redirect{}
	redirect.URL = "https://gitlab.com/mswill/go-url-shortener-app"

	body, err := msgpack.Marshal(&redirect)

	if err != nil {
		log.Fatalln(err)
	}

	resp, err := http.Post(address, "application/x-msgpack", bytes.NewBuffer(body))
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(resp)

	defer resp.Body.Close()

	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatalln(err)
	}

	msgpack.Unmarshal(body, &redirect)

	log.Printf("%v\n", redirect)
}
