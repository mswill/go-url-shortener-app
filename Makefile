rname = "my-redis"
mname = "mng"
redr:
	docker run --name $(rname) -d --rm -v my-redis:/usr/redis  redis

reds:
	docker stop $(rname)

all:
	docker ps -a

wrks:
	docker ps

go:
	go run ./cmd/main.go

#Mongo run instance
mng:
	docker run -d --network pos-net --name $(mname) \
		-v mngdb:/usr/mongodb \
        -e MONGO_INITDB_ROOT_USERNAME=aaa \
        -e MONGO_INITDB_ROOT_PASSWORD=aaa \
        --rm mongo:latest

# stop instance
inmng:
	docker exec -it $(mname) bash


mng-stop:
	docker stop $(mname)
red-stop:
	docker stop $(rname)


stop-all: mng-stop red-stop
